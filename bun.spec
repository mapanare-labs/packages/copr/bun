%global debug_package %{nil}

Name:    bun
Version:        1.1.38
Release: 1%{?dist}
Summary: Bun is an all-in-one toolkit for JavaScript and TypeScript apps.
Group:   Applications/System
License: MIT
Url:     https://github.com/oven-sh/%{name}
Source0: %{url}/releases/download/%{name}-v%{version}/%{name}-linux-x64.zip

%description
Incredibly fast JavaScript runtime, bundler, test runner, and package manager – all in one.

%prep
%setup -qn %{name}-linux-x64

%install
install -d -m 755 %{buildroot}%{_bindir}
install -m 755 %{name} %{buildroot}%{_bindir}

%files
%{_bindir}/%{name}

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Mon Jun 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Wed Apr 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.0.29

* Tue Dec 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.0.20

* Tue Nov 14 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.0.11

* Fri Sep 22 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v1.0.3

* Wed Sep 20 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v1.0.2

* Tue Sep 12 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v1.0.1

* Sat Sep 09 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM
